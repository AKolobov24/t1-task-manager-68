package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.model.Project;

import java.util.Collection;

@RequestMapping("/api/projects")
public interface IProjectRestEndpoint {

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<Project> findAll();

    @Nullable
    @GetMapping("/findById/{id}")
    Project findById(
            @PathVariable("id")
            @NotNull final String id
    );

    @Nullable
    @PostMapping("/save")
    Project save(
            @RequestBody
            @NotNull final Project project
    );

    @PostMapping("/deleteById/{id}")
    void deleteById(
            @PathVariable("id")
            @NotNull final String id
    );

}
