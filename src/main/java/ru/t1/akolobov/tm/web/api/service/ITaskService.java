package ru.t1.akolobov.tm.web.api.service;

import ru.t1.akolobov.tm.web.model.Task;

public interface ITaskService extends IService<Task> {

}
