package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.dto.soap.project.*;

@Endpoint
public final class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";
    public final static String NAMESPACE = "http://t1.ru/akolobov/tm/web/dto/soap/project";

    @Autowired
    private IProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "findAllProjectsRequest", namespace = NAMESPACE)
    public FindAllProjectsResponse findAllProjects(
            @NotNull
            @RequestPayload final FindAllProjectsRequest request
    ) {
        @NotNull final FindAllProjectsResponse response = new FindAllProjectsResponse();
        response.getReturn().addAll(projectService.findAll());
        return response;
    }

    @PayloadRoot(localPart = "findProjectByIdRequest", namespace = NAMESPACE)
    @ResponsePayload
    public FindProjectByIdResponse findProjectById(
            @NotNull
            @RequestPayload final FindProjectByIdRequest request
    ) {
        @NotNull final FindProjectByIdResponse response = new FindProjectByIdResponse();
        System.out.println("request.id = " + request.getId());
        response.setReturn(projectService.findById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "saveProjectRequest", namespace = NAMESPACE)
    public SaveProjectResponse saveProject(
            @NotNull
            @RequestPayload final SaveProjectRequest request
    ) {
        @NotNull final SaveProjectResponse response = new SaveProjectResponse();
        response.setReturn(projectService.save(request.getProject()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "deleteProjectByIdRequest", namespace = NAMESPACE)
    public DeleteProjectByIdResponse deleteProject(
            @NotNull
            @RequestPayload final DeleteProjectByIdRequest request
    ) {
        projectService.deleteById(request.getId());
        return new DeleteProjectByIdResponse();
    }

}
