package ru.t1.akolobov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.dto.soap.task.*;

@Endpoint
public final class TaskSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";
    public final static String NAMESPACE = "http://t1.ru/akolobov/tm/web/dto/soap/task";

    @Autowired
    private ITaskService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "findAllTasksRequest", namespace = NAMESPACE)
    public FindAllTasksResponse findAllTasks(
            @NotNull
            @RequestPayload final FindAllTasksRequest request
    ) {
        @NotNull final FindAllTasksResponse response = new FindAllTasksResponse();
        response.getReturn().addAll(taskService.findAll());
        return response;
    }

    @PayloadRoot(localPart = "findTaskByIdRequest", namespace = NAMESPACE)
    @ResponsePayload
    public FindTaskByIdResponse findTaskById(
            @NotNull
            @RequestPayload final FindTaskByIdRequest request
    ) {
        @NotNull final FindTaskByIdResponse response = new FindTaskByIdResponse();
        System.out.println("request.id = " + request.getId());
        response.setReturn(taskService.findById(request.getId()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "saveTaskRequest", namespace = NAMESPACE)
    public SaveTaskResponse saveTask(
            @NotNull
            @RequestPayload final SaveTaskRequest request
    ) {
        @NotNull final SaveTaskResponse response = new SaveTaskResponse();
        response.setReturn(taskService.save(request.getTask()));
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "deleteTaskByIdRequest", namespace = NAMESPACE)
    public DeleteTaskByIdResponse deleteTask(
            @NotNull
            @RequestPayload final DeleteTaskByIdRequest request
    ) {
        taskService.deleteById(request.getId());
        return new DeleteTaskByIdResponse();
    }

}
